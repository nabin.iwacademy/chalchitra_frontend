import * as actionTypes from '../actionTypes';
import {
    updateObjects
} from '../utility'
import ActionButton from 'antd/lib/modal/ActionButton';

const initialState = {
    token: null,
    error: null,
    loading: false


}



const authStart = (state, action) => {
    return updateObjects(state, {
        error: null,
        loading: true
    });
}


const authSuccess = (state, action) => {
    return updateObjects(state, {
        token: action.token,
        error: null,
        loading: false
    })
}


const authFail = (state, action) => {
    return updateObjects(state, {
        error: action.error,
        loading: false
    })
}


const authLogout = (state, action) => {
    return updateObjects(state, {
        token: null
    })
}


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.AUTH_START:
            return authStart(state, action);
        case actionTypes.AUTH_LOGOUT:
            return authLogout(state, action);
        case actionTypes.AUTH_FAIL:
            return authFail(state, action);
        case actionTypes.AUTH_SUCCESS:
            return authSuccess(state, action);
        default:
            return state

    }
}
export default reducer;