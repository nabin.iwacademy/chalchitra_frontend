import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route
  // Switch,
  // Redirect,
  // Route
} from "react-router-dom";
import Navbar from "./components/Navbar";

import "bootstrap/dist/css/bootstrap.min.css";

import FooterApp from "./components/Footer";
import { connect } from "react-redux";
const HomePage = React.lazy(() => import("./components/HomePage"));
const SingleMoviePage = React.lazy(() =>
  import("./components/singlePage/index")
);
const loginPage = React.lazy(() => import("./components/accounts/login"));
const signupPage = React.lazy(() => import("./components/accounts/register"));
const forgotPassword = React.lazy(() =>
  import("./components/accounts/forgotPassword")
);
const changePassword = React.lazy(() =>
  import("./components/accounts/changePassword")
);
const userDashboard = React.lazy(() =>
  import("./components/Dashboard/userDashboard")
);

function App() {
  return (
    <Router>
      <Navbar> </Navbar>

      <React.Suspense fallback={"loading ......."}>
        <Switch>
          <Route path="/" exact component={HomePage}></Route>
          <Route path="/movies/:id" exact component={SingleMoviePage}></Route>
          <Route path="/login" exact component={loginPage}></Route>
          <Route path="/signup" component={signupPage}></Route>
          <Route path="/forgotPassword" component={forgotPassword}></Route>
          <Route path="/changePassword" component={changePassword}></Route>
          <Route path="/userDashboard" component={userDashboard}></Route>
        </Switch>
      </React.Suspense>

      <FooterApp> </FooterApp>
    </Router>
  );
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.token !== null
  };
};

export default connect(mapStateToProps)(App);
