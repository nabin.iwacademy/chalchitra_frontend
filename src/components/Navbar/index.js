import React, { Component } from "react";
import { Navbar, Nav, Container, Form, FormControl } from "react-bootstrap";
import { NavWrapper } from "./style";
import { Link } from "react-router-dom";
import { FaSearch } from "react-icons/fa";
import PropTypes from "prop-types";
import styled from "styled-components";

export default class Navbars extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true,
      scrollPos: 0
    };
    this.handleScroll = this.handleScroll.bind(this);
  }

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll() {
    const { scrollPos } = this.state;
    this.setState({
      scrollPos: document.body.getBoundingClientRect().top,
      show: document.body.getBoundingClientRect().top > scrollPos
    });
  }

  render() {
    return (
      <NavWrapper>
        <StyledNavbar className={this.state.show ? "active" : "hidden"}>
          <Container>
            <Navbar expand="lg" inverse fixedTop>
              <Link to="/">
                <img src="/img/logo.png" alt="Logo"></img>
              </Link>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              {/* <Form inline>
          <FaSearch />
          <FormControl type="text" placeholder="Search" className="mr-sm-2" />
        </Form> */}
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto">
                  <Link to="/" className="nav-link">
                    HOME
                  </Link>
                  <Link to="/changePassword" className="nav-link">
                    THREATERS
                  </Link>
                  <Link to="/" className="nav-link">
                    CONTACT
                  </Link>
                  <Link to="/signup" className="nav-link join-us-button">
                    JOIN US
                  </Link>
                </Nav>
              </Navbar.Collapse>
            </Navbar>
          </Container>
        </StyledNavbar>
      </NavWrapper>
    );
  }
}
const StyledNavbar = styled.div`
  position: fixed;
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  background: #0a1e5e;
  z-index: 1000;
`;
