import styled from "styled-components";


export const NavWrapper = styled.div `
   

background-color: #0a1e5e;
font-family: 'Open Sans', sans-serif;
form{
    background-color: #fff;
    margin-left:40px;
    padding: 0 20px;
    border-radius: 100px;
    input{
        border:none;
    }
    input:focus{
        box-shadow:none;
    }

}
.nav-link{
    color:#fff !important;
}
.container{
    height: 100px;
    padding-top: 17px;
     
}
.join-us-button{
   
        background-image: -webkit-linear-gradient(169deg, #5560ff 17%, #aa52a1 63%, #ff4343 100%);
        padding: 10px 49px;
        font-weight: 600;
        border-radius: 25px;
        display: inline-block;
        width:130px;
        text-align: -webkit-center;
      

}

.join-us-button:hover{
   
   opacity: 50%

}

.active {
    visibility: visible;
    transition: all 200ms ease-in;
  }
  .hidden {
    visibility: hidden;
    transition: all 200ms ease-out;
    transform: translate(0, -100%);
  }


`;