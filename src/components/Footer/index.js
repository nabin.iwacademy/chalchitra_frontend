import React, { Component } from "react";
import { Form, FormControl } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./style.css";

class FooterApp extends Component {
  render() {
    return (
      <div className="footer-section">
        <div className="newslater-section padding-bottom">
          <div className="container">
            <div className="newslater-container">
              <div className="newslater-wrapper ">
                <h5 className="cate"> subscribe to Chalchitra </h5>
                <h3 className="title"> to get exclusive benifits </h3>
                <Form className="newslater-form">
                  <FormControl type="text" placeholder="Your Email Address" />
                  <button type="submit"> subscribe </button>
                </Form>
                <p>We respect your privacy, so we never share your info</p>
              </div>
            </div>
          </div>
        </div>
        {/* footer logo */}
        <div className="container">
          <div className="footer-top">
            <div className="logo">
              <Link to="/">
                <img src="/img/logo.png" alt="Logo"></img>
              </Link>
            </div>
            <ul className="social-icons">
              <li>
                <Link to="/">
                  <i className="fab fa-facebook-f"></i>
                </Link>
              </li>

              <li>
                <Link to="/">
                  <i className="fab fa-twitter"></i>
                </Link>
              </li>
              <li>
                <Link to="/">
                  <i className="fab fa-pinterest-p"></i>
                </Link>
              </li>
              <li>
                <Link to="/">
                  <i className="fab fa-instagram"></i>
                </Link>
              </li>
            </ul>
          </div>
          <div className="footer-bottom">
            <div className="footer-bottom-area">
              <div className="left">
                <p>
                  Copyright © 2020.All Rights Reserved By{" "}
                  <Link to="/">Chalchitra </Link>
                </p>
              </div>
              <ul className="links">
                <li>
                  <Link to="/">About</Link>
                </li>
                <li>
                  <Link to="/">Terms Of Use</Link>
                </li>
                <li>
                  <Link to="/">Privacy Policy</Link>
                </li>
                <li>
                  <Link to="/">FAQ</Link>
                </li>
              </ul>
            </div>
          </div>
        </div>

        {/* footer logo end */}
      </div>
    );
  }
}

export default FooterApp;
