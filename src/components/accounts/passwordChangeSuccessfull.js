import React, { Component } from "react";
import "./style.css";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
export default class SignUp extends Component {
  render() {
    return (
      <Container className="custom-container">
        <form className="custom-form">
          <h4>Password Change Successfully</h4>
          <p className="forgot-password">
            Please<Link to="/forgotPassword">Login?</Link>
          </p>
        </form>
      </Container>
    );
  }
}
