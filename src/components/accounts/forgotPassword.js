import React, { Component } from "react";
import "./style.css";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
export default class SignUp extends Component {
  render() {
    return (
      <Container className="custom-container">
        <form className="custom-form">
          <h4>Please Enter your email to reset Password</h4>

          <div className="form-group">
            <label>Email address</label>
            <input
              type="email"
              className="form-control"
              placeholder="Enter email"
              required
            />
          </div>

          <button type="submit" className="btn btn-primary btn-block">
            Submit
          </button>
        </form>
      </Container>
    );
  }
}
