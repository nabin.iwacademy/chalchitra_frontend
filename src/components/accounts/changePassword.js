import React, { Component } from "react";
import "./style.css";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
export default class SignUp extends Component {
  render() {
    return (
      <Container className="custom-container">
        <form className="custom-form">
          <h4>Changing Password</h4>

          <div className="form-group">
            <label>Old Password</label>
            <input
              type="email"
              className="form-control"
              placeholder="Old Password"
              required
            />
          </div>
          <hr />
          <div className="form-group">
            <label>New Password</label>
            <input
              type="password"
              className="form-control"
              placeholder="Enter New Password"
              required
            />
          </div>
          <div className="form-group">
            <label>Conform Password</label>
            <input
              type="password"
              className="form-control"
              placeholder="Enter Conform Password"
              required
            />
          </div>

          <button type="submit" className="btn btn-primary btn-block">
            Submit
          </button>
        </form>
      </Container>
    );
  }
}
