import React, { useEffect, useState } from "react";
import Axios from "axios";
import { Container, Col, Row } from "react-bootstrap";
function SinglePage(props) {
  const [movie, setmovie] = useState({});
  useEffect(() => {
    let apiKey = "48b41efd232df2c87d998f410855acd4";

    Axios.get(
      `https://api.themoviedb.org/3/movie/${props.match.params.id}?api_key=${apiKey}&language=en-US`
    )
      .then(res => setmovie(res.data))
      .catch(err => console.log(err));
  }, []);
  return (
    <Container className="mt-5">
      <Row>
        <Col md="6">
          <img
            src={`https://image.tmdb.org/t/p/w500/${movie.poster_path}`}
            alt=""
            srcset=""
          />
        </Col>
        <Col md="6">
          <h1>{movie.title}</h1>
          <p>{movie.overview}</p>
          <ul>
            <li>Revenue:{movie.revenue}</li>
            <li>Popularity:{movie.popularity}</li>
            <li>Original Language:{movie.original_language}</li>
            <li>
              Generes:
              {Array.isArray(movie.genres) &&
                movie.genres.map(el => <span>{el.name},</span>)}
            </li>
          </ul>
        </Col>
      </Row>
    </Container>
  );
}

export default SinglePage;
