import React from "react";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";

export default function UpcomingMovie({ movie }) {
  var settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1
  };
  return (
    <Container>
      <div className="clearfix mb-2">
        <h4 className="float-left mt-5">Popular Movies </h4>{" "}
        <Link to="/" className="float-right text-uppercase mt-5">
          See All{" "}
        </Link>{" "}
      </div>{" "}
      <Slider {...settings}>
        {" "}
        {movie.map(function(movie) {
          return (
            <React.Fragment>
              <Link to={`/movies/${movie.id}`}>
                <Col>
                  <Card>
                    <Card.Img
                      variant="top"
                      src={`https://image.tmdb.org/t/p/w500/${movie.poster_path}`}
                    />{" "}
                    <Card.Body>
                      <span> {movie.title} </span>{" "}
                    </Card.Body>{" "}
                  </Card>{" "}
                </Col>{" "}
              </Link>{" "}
            </React.Fragment>
          );
        })}{" "}
      </Slider>{" "}
    </Container>
  );
}
