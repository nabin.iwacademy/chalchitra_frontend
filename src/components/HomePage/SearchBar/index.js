import React, { Component } from "react";
import { Form, FormControl } from "react-bootstrap";
import "./../../Footer/style.css";
class SearchBox extends Component {
  render() {
    return (
      <div className="search-ticket-section padding-top pt-lg-0 ">
        <div className="container">
          <div
            className="search-tab bg_img"
            style={{ backgroundImage: "URL(/img/ticket-bg01.jpg)" }}
          >
            <div className="row align-items-center mb--20">
              <div className="col-lg-6 mb-20">
                <div className="search-ticket-header">
                  <h6 class="category">welcome to Chalchitra </h6>
                  <h3 class="title">what are you looking for</h3>
                </div>
              </div>
              <div className="col-lg-6 mb-20">
                <ul className="tab-menu ticket-tab-menu">
                  <li className="active">
                    <div className="tab-thumb">
                      <img src="/img/ticket-tab01.png" alt="ticket" />
                    </div>
                    <span>movie</span>
                  </li>
                </ul>
              </div>
            </div>
            <div className="tab-area">
              <div className="tab-item active">
                <form className="ticket-search-form">
                  <div className="form-group large">
                    <input type="text" placeholder="Search for Movies" />
                    <button type="submit">
                      <i className="fas fa-search"></i>
                    </button>
                  </div>

                  <div className="form-group">
                    <div className="thumb">
                      <img src="/img/city.png" alt="ticket" />
                    </div>
                    <span className="type">city</span>
                    <select className="select-bar">
                      <option value="london">London</option>
                      <option value="dhaka">dhaka</option>
                      <option value="rosario">rosario</option>
                      <option value="madrid">madrid</option>
                      <option value="koltaka">kolkata</option>
                      <option value="rome">rome</option>
                      <option value="khoksa">khoksa</option>
                    </select>
                  </div>
                  <div className="form-group">
                    <div className="thumb">
                      <img src="/img/date.png" alt="ticket" />
                    </div>
                    <span className="type">date</span>
                    <select className="select-bar">
                      <option value="26-12-19">23/10/2020</option>
                      <option value="26-12-19">24/10/2020</option>
                      <option value="26-12-19">25/10/2020</option>
                      <option value="26-12-19">26/10/2020</option>
                    </select>
                  </div>
                  <div className="form-group">
                    <div className="thumb">
                      <img src="/img/cinema.png" alt="ticket" />
                    </div>
                    <span className="type">cinema</span>
                    <select className="select-bar">
                      <option value="Awaken">Awaken</option>
                      <option value="dhaka">dhaka</option>
                      <option value="rosario">rosario</option>
                      <option value="madrid">madrid</option>
                      <option value="koltaka">kolkata</option>
                      <option value="rome">rome</option>
                      <option value="khoksa">khoksa</option>
                    </select>
                  </div>
                </form>
              </div>
              <div className="tab-item"></div>
              <div className="tab-item"></div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SearchBox;
