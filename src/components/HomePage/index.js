import React, { useEffect, useState } from "react";
import styled from "styled-components";
import Axios from "axios";
import NowPlaying from "./NowPlaying";
import { HomeWrapper } from "./style";
import UpcomingMovie from "./UpcomingMovies";
import TopratedMovie from "./TopRatedMovies";
import IntroductionBar from "./InroductionBar";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import SearchBox from "./SearchBar";
import "./../Footer/style.css";

export default function HomePage() {
  const [nowPlaying, setNowPlaying] = useState([]);
  const [upcomingMovies, setUpcomingMovies] = useState([]);
  const [topRatedMovies, setTopRatedMovies] = useState([]);
  useEffect(() => {
    getAllMovies();
  }, []);
  const getAllMovies = async () => {
    let apiKey = "48b41efd232df2c87d998f410855acd4";
    const allMovies = await Axios.all([
      Axios.get(
        `https://api.themoviedb.org/3/movie/now_playing?api_key=${apiKey}&language=en-US&page=1`
      ),
      Axios.get(
        `https://api.themoviedb.org/3/movie/upcoming?api_key=${apiKey}&language=en-US&page=1`
      ),
      Axios.get(
        `https://api.themoviedb.org/3/movie/top_rated?api_key=${apiKey}&language=en-US&page=1`
      )
    ]);

    setNowPlaying(allMovies[0].data.results);
    setUpcomingMovies(allMovies[1].data.results);
    setTopRatedMovies(allMovies[2].data.results);
  };
  return (
    <HomeWrapper>
      <IntroductionBar> </IntroductionBar>
      <SearchBox></SearchBox>

      {/* <NowPlaying movie={nowPlaying} />
      <PopularMovie movie={popularMovies}> </PopularMovie>{" "}
      <TopratedMovie movie={topRatedMovies}> </TopratedMovie>  */}
      <div className="movie-section padding-top padding-bottom bg-two">
        <div className="container">
          <div className="row flex-wrap-reverse justify-content-center">
            <div className="col-lg-3 col-sm-10  mt-50 mt-lg-0">
              <div className="widget-1 widget-facility">
                <div className="widget-1-body">
                  <ul>
                    <li>
                      <Link href="#">
                        <span class="img">
                          <img src="/img/sidebar01.png" alt="sidebar" />
                        </span>
                        <span class="cate">24X7 Care</span>
                      </Link>
                    </li>
                    <li>
                      <Link href="#0">
                        <span class="img">
                          <img src="/img/sidebar02.png" alt="sidebar" />
                        </span>
                        <span class="cate">100% Assurance</span>
                      </Link>
                    </li>
                    <li>
                      <Link href="#0">
                        <span class="img">
                          <img src="/img/sidebar03.png" alt="sidebar" />
                        </span>
                        <span class="cate">Our Promise</span>
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="widget-1 widget-trending-search">
                <h3 className="title">Trending Searches</h3>
                <div className="widget-1-body">
                  <ul>
                    <li>
                      <h6 class="sub-title">
                        <a href="#0">mars</a>
                      </h6>
                      <p>Movies</p>
                    </li>
                    <li>
                      <h6 class="sub-title">
                        <a href="#0">alone</a>
                      </h6>
                      <p>Movies</p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-lg-9">
              <div className="article-section padding-bottom">
                <div className="section-header-1">
                  <h2 class="title">Now playing </h2>
                  <a class="view-all" href="/">
                    View All
                  </a>
                </div>
                <div className="row justify-content-center">
                  <NowPlaying movie={nowPlaying} />
                </div>
              </div>

              <div className="article-section padding-bottom">
                <div className="section-header-1">
                  <h2 class="title">Upcoming </h2>
                  <a class="view-all" href="/">
                    View All
                  </a>
                </div>
                <div className="row justify-content-center">
                  <NowPlaying movie={nowPlaying} />
                </div>
              </div>
              <div className="article-section"></div>
            </div>
          </div>
        </div>
      </div>
    </HomeWrapper>
  );
}
