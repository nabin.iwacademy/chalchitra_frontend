import React, { Component } from "react";
import "./style.css";

class IntroductionBar extends Component {
  render() {
    return (
      <div className="home-intro">
        <div className="bg-image"></div>
        <div className="container">
          <div className="banner-content">
            <h1 class="title  cd-headline clip">
              <span class="d-block">book your</span> tickets for
              <span class="color-theme cd-words-wrapper p-0 m-0">
                <b class="is-visible">Movie</b>
              </span>
            </h1>
            <p>
              Safe, secure, reliable ticketing.Your ticket to live
              entertainment!
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default IntroductionBar;
