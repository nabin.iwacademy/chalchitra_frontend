import React from "react";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import "./../../Footer/style.css";
import "./style.css";

export default function NowPlaying({ movie }) {
  var settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1
  };
  return (
    <div className="container">
      <Slider {...settings}>
        {movie.map(function(movie) {
          return (
            <React.Fragment>
              <Link to={`/movies/${movie.id}`}>
                <Card style={{ width: "14.5rem", height: "500px" }}>
                  <Card.Img
                    variant="top"
                    src={`https://image.tmdb.org/t/p/w500/${movie.poster_path}`}
                  />
                  <Card.Body className="card-body">
                    <Card.Title>{movie.title}</Card.Title>
                    <p>
                      <span>
                        <i class="far fa-star"></i>
                      </span>
                      <span>
                        <i class="fas fa-star text-warning"></i>
                      </span>
                    </p>
                    <Link to="/">
                      <Button variant="success">Details</Button>
                    </Link>
                  </Card.Body>
                </Card>
              </Link>
            </React.Fragment>
          );
        })}
      </Slider>
    </div>
  );
}
